package oop.example;

import java.util.Scanner;

public class Exercises implements General {
	private int n;
	private int[] arr;
	private int size;

	public int getN() {
		return n;
	}

	public void setN(int n) {
		this.n = n;
	}

	public int[] getArr() {
		return arr;
	}

	public void setArr(int[] arr) {
		this.arr = arr;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	@SuppressWarnings("resource")
	@Override
	public void inputData() {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Input n = ");
		n = scanner.nextInt();
	}

	@Override
	public boolean checkPrime(int n) {
		if (n < 2) {
			return false;
		} else {
			for (int i = 2; i <= Math.sqrt(n); i++) {
				if (n % i == 0) {
					return false;
				}
			}
		}
		return true;
	}

	@Override
	public void showListPrime() {
		System.out.printf("List Prime < n: ");
		for (int i = 0; i < n; i++) {
			if (checkPrime(i) == true) {
				System.out.print(i + " ");
			}
		}

	}

	@Override
	public void showListCharacter() {
		String str = "" + n; 
		size = str.length();
		arr = new int[size];
		int i = 0;
		do {
			arr[i] = n % 10;
			n /= 10;
			i++;
		} while (n != 0);
		System.out.printf("\nList Character from n: ");
		for (i = size - 1; i >= 0; i--) {
			System.out.print(arr[i] + " ");
		}

	}

	@Override
	public int Calculate() {
		return 0;

	}

}
