package oop.example;

public interface General {
	void inputData();
	public boolean checkPrime(int n);
	void showListPrime();
	void showListCharacter();
	int Calculate();
}
